#include <iostream>

using namespace std;

void inc(int a)
{
	a++;
}

void _inc(int &a)
{
	a++;
}

void __inc(int *a)
{
	*a = *a + 1;
}

int main()
{
	int a = 0;

	cout << "Before call a = " << a << "\n";
	inc(a);
	cout << "After call a = " << a << "\n";

	cout << "Before call a = " << a << "\n";
	_inc(a);
	cout << "After call a = " << a << "\n";

	cout << "Before call a = " << a << "\n";
	__inc(&a);
	cout << "After call a = " << a << "\n";
}

