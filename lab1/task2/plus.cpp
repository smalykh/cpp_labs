#include <iostream>

namespace n1 
{
	int plus(int a, int b)
	{
		return a + b;
	}
}

namespace n2
{
	int plus(int a, int b)
	{
		return (a + b) % 2;
	}
}

int main()
{
	std::cout << "case 1: " << n1::plus(1, 2) << std::endl;
	std::cout << "case 2: " << n2::plus(1, 2) << std::endl;
}
