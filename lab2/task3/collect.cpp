#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

class Collection
{
protected:
	int *items;
	int size;
public:
	int get(int i)
	{
		if ((i < 0) || (i >= size))
			return -1;
		else
			return items[i];
	}

	virtual void put(int element) = 0;
	virtual int take() = 0;

	Collection(int n)
	{
		size = n;
		items = new int[n];
	}

	~Collection()
	{
		delete[] items;
	}
};

class Stack: public Collection
{
	int p;
public:
	Stack(int size): Collection(size)
	{	
		p = 0;
	}

	void put(int element) override
	{
		if (p < size)
			items[p++] = element;
	}

	int take() override
	{
		int res;

		if (p > 0)
		{
			res = items[--p];
			items[p] = -1;
			return res;
		}
		else
			return -1;
	}
};

class Queue: public Collection
{
	int head, tail, n;
public:
	Queue(int size): Collection(size)
	{	
		head = tail = n = 0;
	}

	void put(int element) override
	{
		if (n >= size)
			return;

		items[head++] = element;
		n++;

		if (head >= size)
			head = 0;
	}

	int take() override
	{
		int res;

		if (n <= 0)
			return -1;

		res = items[tail];
		items[tail++] = -1;
		n--;

		if (tail >= size)
			tail = 0;

		return res;
	}
};

int main(int argc, char **argv)
{
	int i, j;
	Collection **c = new Collection*[10];
	std::srand(std::time(nullptr));

	for (i = 0; i < 10; i++)
	{
		if (std::rand() % 2)
			c[i] = new Stack(10);
		else
			c[i] = new Queue(10);
	}

	for (i = 0; i < 10; i++)
		for (j = 0; j < 10; j++)
			c[i]->put(std::rand() % 100);

	for (i = 0; i < 10; i++)
		for (j = 0; j < 5; j++)
			c[i]->take();

	for (i = 0; i < 10; i++)
	{
		cout << "collection " << i << ":" << endl;
		for (j = 0; j < 10; j++)
			cout << " " << c[i]->get(j);
		cout << endl;
	}

	for (i = 0; i < 10; i++)
		delete c[i];

	delete[] c;
	return 0;
}
