#include <iostream>
using namespace std;

class Expr
{
public:
	virtual int eval() = 0;
};

class Num : public Expr
{
private:
	int number;
public:
	Num(int n)
	{	
		number = n;
	}

	int eval() override
	{
		return number;
	}
};

class Add : public Expr
{
	Expr *left;
	Expr *right;
public:
	Add(Expr *l, Expr *r)
	{	
		left = l;
		right = r;
	}

	int eval() override
	{
		return left->eval() + right->eval();
	}
};

class Substract : public Expr
{
	Expr *left;
	Expr *right;
public:
	Substract(Expr *l, Expr *r)
	{	
		left = l;
		right = r;
	}

	int eval() override
	{
		return left->eval() - right->eval();
	}
};

int main(int argc, char **argv)
{
	Expr *res = new Add(new Substract(new Num(7), new Num(2)), new Add(new Num(3), new Num(1)));
	cout << "Result: " <<  res->eval() << endl;
	delete res;
	return 0;
}
