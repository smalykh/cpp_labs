#include <iostream>
using namespace std;

template <class T>
T min(T &a, T &b)
{
	if (a < b)
		return a;
	else
		return b;
}

int main()
{
	cout << min<int>(3,4) << endl;
}
