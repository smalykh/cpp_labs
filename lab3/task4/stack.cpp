#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

template <class T>
class Stack {
	T *items;
	int size;
	int p;
public:
	Stack(int n)
       	{
		p = 0;
		size = n;
		items = new T[size];
	}

	~Stack()
	{
		while (p)
			pop();
	}

	Stack& operator+=(Stack const &s)
	{
		for (int i = 0; i < s.p; i++)
			this->push(s.items[i]);
		return *this;
	}

        friend Stack operator+(Stack lhs, const Stack& rhs)
	{
		lhs += rhs;
		return lhs;
	}

	void push(const T& object)
	{
		if (p < size)
			items[p++] = object;
		else
			throw "Stack is full";
	}

	T pop()
	{
		//delete items[--p];
		p--;
	}

	const T& top()
	{
		if (p > 0)
			return items[p-1];
		else
			throw "Stack is empty";
	}

};

int main()
{
	int i, a;

	srand(time(nullptr));

	Stack<int> s = Stack<int>(10);
	Stack<int> s0 = Stack<int>(10);

	cout << "Push to s:" << endl;
	for (i = 0; i < 5; i++)
	{
		a = rand() % 10;
		cout << " " << a;
		s.push(a);
	}
	cout << endl;
	
	cout << "Push to s0:" << endl;
	for (i = 0; i < 5; i++)
	{
		a = rand() % 10;
		cout << " " << a;
		s0.push(a);
	}
	cout << endl;

	Stack<int> res = s + s0;

	cout << "Pop (s + s0):" << endl;
	for (i = 0; i < 10; i++)
	{
		cout << " " << res.top();
		res.pop();
	}
	cout << endl;
}
