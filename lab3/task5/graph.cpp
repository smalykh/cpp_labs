#include <iostream>
#include <list>
#include<queue>

using namespace std;

class Graph
{
	int nodes;
	vector<int>* adjMat;
	bool *visited;
	
	void bfs(int n)
	{
		for(int i = 0; i < nodes; i++)
			visited[i] = false;

		vector<int>::iterator it;
		queue<int> q;

		q.push(n);
		while (!q.empty()) 
		{
			int current = q.front();
			visited[current] = true;
			q.pop();

			for (it = adjMat[current].begin(); it != adjMat[current].end(); it++)
			{
				if(!visited[*it])
					q.push(*it);
			}
		}
	}

	void checkSanity(int nodeid)
	{
		if ((nodeid < 0) || (nodeid >= nodes))
			throw invalid_argument("");
	}

public:
	Graph(const vector<int> &starts, const vector<int> &ends)
	{
		int i;

		if (starts.size() != ends.size())
			throw invalid_argument("");

		nodes = starts.size();
		adjMat = new vector<int>[nodes];
		visited = new bool[nodes];

		for (i = 0; i < starts.size(); i++)
		{
			int u = starts[i];
			int v = ends[i];

			adjMat[u].push_back(v);
		}
	}

	int numOutgoing(const int nodeId)
	{
		int i, count = 0;

		checkSanity(nodeId);
		bfs(nodeId);
		for (i = 0; i < nodes; i++) {
			if (visited[i])
				count++;
		}
		return count;
	}

	vector<int>& adjacent(const int nodeId)
	{
		int i;
		vector<int> *vec = new vector<int>();

		checkSanity(nodeId);
		bfs(nodeId);
		for (i = 0; i < nodes; i++) {
			if (visited[i])
				vec->push_back(i);
		}
		return *vec;
	}
};

void show_info(Graph& g, int v)
{
	cout << "Information about V" << v << endl;
	cout << "Amount: " << g.numOutgoing(v) << endl;
	cout << "Numbers: " << endl;

	vector<int> outgoing = g.adjacent(v);
	for (int x : outgoing)
		cout << " " << x;

	cout << endl;
}

int main()
{
	//     2
	//    /|\
	//   0 | 3
	//    \|
	//     1
	vector<int> starts = vector<int> {0, 0, 1, 2, 2, 3};
	vector<int> ends   = vector<int> {1, 2, 2, 0, 3, 3};

	Graph g = Graph(starts, ends);
	show_info(g, 0);
	show_info(g, 1);
	show_info(g, 2);
	show_info(g, 3);
}
