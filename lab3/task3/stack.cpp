#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

template <class T>
class Stack {
	T *items;
	int size;
	int p;
public:
	Stack(int n)
       	{
		p = 0;
		size = n;
		items = new T[size];
	}

	~Stack()
	{
		while (p)
			pop();
	}

	void push(const T& object)
	{
		if (p < size)
			items[p++] = object;
		else
			throw "Stack is full";
	}

	T pop()
	{
		//delete items[--p];
		p--;
	}

	const T& top()
	{
		if (p > 0)
			return items[p-1];
		else
			throw "Stack is empty";
	}

};

int main()
{
	int i, a;

	srand(time(nullptr));

	Stack<int> s = Stack<int>(10);

	cout << "Push:" << endl;
	for (i = 0; i < 5; i++)
	{
		a = rand() % 10;
		cout << " " << a;
		s.push(a);
	}
	cout << endl;

	cout << "Pop:" << endl;
	for (i = 0; i < 5; i++)
	{
		cout << " " << s.top();
		s.pop();
	}
	cout << endl;
}
